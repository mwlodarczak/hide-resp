#!/usr/bin/env python3

import os
from glob import glob
import subprocess

RESAMP_FREQ = 100
DATA_DIR = os.path.expanduser('~/Data/hide/')

for wav_path in glob(os.path.join(DATA_DIR, '*', '*_SUM.wav')):
    wav_resamp_path = wav_path.replace('.wav', '_{}Hz.wav'.format(RESAMP_FREQ))
    subprocess.call(['sox', wav_path, '-r', str(RESAMP_FREQ), wav_resamp_path])
