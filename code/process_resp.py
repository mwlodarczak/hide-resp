#!/usr/bin/env python3

import os
import sys
from glob import glob

# import numpy as np
# import matplotlib.pyplot as plt

import tgt
from rip import Resp


def main(resp_dir, speech_dir, outdir):

    for resp_path in glob(os.path.join(resp_dir, '*', '*_SUM_100Hz.wav')):

        file_id = os.path.basename(resp_path).replace('_SUM_100Hz.wav', '')
        print('Processing {}'.format(file_id), file=sys.stderr)

        session_id, speaker_id = file_id.split('_')

        speech_path = os.path.join(speech_dir, session_id + '.TextGrid')
        speech_tier = tgt.read_textgrid(speech_path).get_tier_by_name(
            'Channel' + speaker_id[-1])
        speech_tier.delete_annotations_with_text('s')

        resp = Resp.from_wav(resp_path, speech=speech_tier)
        resp.find_cycles()
        resp.samples = resp.samples - resp.estimate_rel(method='dynamic')
        # resp.find_holds()

        tg_outpath = os.path.join(outdir, file_id + '.TextGrid')
        resp.save_annotations(tg_outpath, tiers=['cycles'])

        # peaks = np.array(resp.peaks * resp.samp_freq, dtype=int)
        # vals = np.array(resp.troughs * resp.samp_freq, dtype=int)
        # plt.plot(resp.samples)
        # for hold in resp.holds:
        #     plt.axvspan(hold.start_time * resp.samp_freq,
        #                 hold.end_time * resp.samp_freq,
        #                 alpha=0.5)
        # plt.plot(peaks, resp[peaks], 'mo')
        # plt.plot(vals, resp[vals], 'co')
        # plt.show()


if __name__ == '__main__':
    main(os.path.expanduser('~/Data/hide'),
         '../data/speech', '../data/resp')
