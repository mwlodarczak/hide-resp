#!/usr/bin/env python3

import shutil
import os
import sys


WAV_DIR = os.path.expanduser('~/Data/hide')
SPEECH_INDIR = '../data/speech_org'
SPEECH_OUTDIR = '../data/speech/'

session_ids = sorted(f.name for f in os.scandir(WAV_DIR) if f.is_dir())

for i, session_id in enumerate(session_ids):

    speaker1_id = 1 + i * 2
    speaker2_id = speaker1_id + 1
    session_id_old = 'S{:02d}oS{:02d}'.format(speaker1_id, speaker2_id)

    source_path = os.path.join(SPEECH_INDIR, session_id_old + '.TextGrid')
    target_path = os.path.join(SPEECH_OUTDIR, session_id + '.TextGrid')

    assert os.path.exists(source_path)

    print('Copying: {} -> {}'.format(session_id, session_id_old),
          file=sys.stderr)
    shutil.copyfile(source_path, target_path)
